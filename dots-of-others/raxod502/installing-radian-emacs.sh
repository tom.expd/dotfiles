safe_link() {
  if [[ -e "$2" && ! -L "$2" ]] ; then
    echo "already exists and not a symlink: $2" >&2
    exit 1
  else
    ln -sf "$1" "$2"
  fi
}

# bash
  # yarn global add bash-language-server
  bash=(bash-language-server)
# Flow
  # yarn global add flow-bin
  flow=(flow-bin)
# C/C++
  c=(clang)
# Haskell
  haskell=(haskell-ide-engine)
# HTML
  html=(vscode-html-languageserver-bin)
# JavaScript/TypeScript
  javascript=(prettier typescript-language-server-bin)
# Python - the language server is downloaded automatically courtesy of lsp-python-ms.
  python=(python-black)
# LaTeX
  config_latex () {
    wget -O ~/.local/bin/texlab.jar https://github.com/latex-lsp/texlab/releases/download/v0.4.2/texlab.jar
  }
# Golang - add "${GOPATH}"/bin to your "${PATH}"
  config_golang () {
    go get -u golang.org/x/tools/gopls
  }

yay -S "${bash[@]}" "${html[@]}" "${python[@]}" # "${flow[@]}" "${c[@]}" "${haskell[@]}" "${javascript[@]}"
config_golang
# config_latex

mkdir -p "${HOME}"/.emacs.d/straight/versions

safe_link $(pwd)/radian/emacs/early-init.el "${HOME}"/.emacs.d/early-init.el
safe_link $(pwd)/radian/emacs/init.el "${HOME}"/.emacs.d/init.el
safe_link $(pwd)/radian/emacs/versions.el "${HOME}"/.emacs.d/straight/versions/radian.el