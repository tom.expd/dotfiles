# source '"${HOME}"/.shrc.d/shrc.sh' if it exists
if [ -e "${HOME}"/.shrc.d/ ] ; then
  . "${HOME}"/.shrc.d/shrc.sh >/dev/null 2>&1
  . "${HOME}"/.shrc.d/path.sh >/dev/null 2>&1
  while IFS= read -r -d '' f ; do
    . "${f}" >/dev/null 2>&1
  done < <(find "${HOME}"/.shrc.d/* '!' -name 'shrc.sh' '!' -name 'path.sh' -print0)
fi

# Start zsh framework, only if using zsh
if [[ "$(ps -ef | awk '$2==pid' pid=$$ | awk -F/ '{print $NF}')" == zsh ]] ; then
  # start zim
  if [ -e "${HOME}"/.zim/ ] ; then
    export ZIM_HOME="${ZDOTDIR:-${HOME}}"/.zim
    [[ -s "${ZIM_HOME}"/init.zsh ]] && . "${ZIM_HOME}"/init.zsh
  fi
fi