#+ python
#- pyenv
if [ -e "${HOME}"/.pyenv/bin/ ] ; then
  eval "$(pyenv init -)"
  eval "$(pyenv virtualenv-init -)"
fi