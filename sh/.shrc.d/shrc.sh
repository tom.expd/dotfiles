# If not running interactively, don't do anything
[[ "$-" != *i* ]] && return

# TODO: Check if running inside emacs to remove extra prompts from emacs. (may have to do this within emacs and not shell)
# if [ -n "$INSIDE_EMACS" ] ; then
#   # export PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*} $(eval "echo ${MYPSDIR}")\007"'
#  # else
#   export PROMPT_COMMAND=''
# fi

# Define functions
## concatonate paths
pathconcat () {
  if ! echo "${PATH}" | grep -Eq "(^|:)${1}($|:)" ; then
    if [ "${2}" = "after" ] ; then
      PATH="${PATH}":"${1}"
    else
      PATH="${1}":"${PATH}"
    fi
  fi
}

## check for gnu packages on MacOS
gnu_path_check () {
    if [ -d /usr/local/opt/"${1}"/libexec/gnubin ] ; then
      pathconcat /usr/local/opt/"${1}"/libexec/gnubin
    fi
}

## :WIP: Use gnu man pages for gnu packages on MacOS
man_swap () {
  find_alias="$(alias | grep "${1}.*=")"
  if [[ "${find_alias}" ]] ; then
    echo "${find_alias}"
  else
    # find_pkg="$(echo "${PATH}" | awk -F: -v pkg="[$1]" '{for(i=1;i<=NF;i++){if ($i ~ /pkg/){print $i}}}' | awk -F/ -v pkg="[$1]" '{for(i=1;i<=NF;i++){if ($i ~ /pkg/){print $i}}}')"
    find_pkg="$(echo "${PATH}" | grep -E "(${1}|IRF)[^:]*")"
    if [[ "${find_pkg}" ]] ; then
      echo "${find_pkg}"
    else
      echo 'false'
    fi
  fi
}

# Add support for 256 colors
export TERM="xterm-256color"